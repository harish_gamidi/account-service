package com.accounts.service;

import com.accounts.controller.AccountsController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class AccountsApplicationTests {

    @Autowired
    private AccountsController accountsController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenAccountsControllerInjected_thenNotNull() throws Exception {
        Assertions.assertThat(accountsController).isNotNull();
    }

    @Test
    public void whenGetAccounts_thenEmptyList() throws Exception {
//        Assert 404
        mockMvc.perform(MockMvcRequestBuilders
                .get("/accounts")
                .queryParam("customerId", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void whenAccountCreated_thenAccountListNotEmpty() throws Exception {

        // Create account

        mockMvc.perform(MockMvcRequestBuilders.post("/accounts/current")
                .content("{\"customerId\": \"1\", \"initialCredit\":\"0.0\"}")
                .contentType(MediaType.APPLICATION_JSON)).
                andExpect(MockMvcResultMatchers.status().isOk());

        // Assert that there is a new account created for this customer now
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders
                .get("/accounts")
                .queryParam("customerId", "1")
                .contentType(MediaType.APPLICATION_JSON));
        response.andExpect(MockMvcResultMatchers.status().isOk());
    }
}
