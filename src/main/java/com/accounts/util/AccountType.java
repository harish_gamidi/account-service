package com.accounts.util;

public enum AccountType {
    SAVINGS,
    CURRENT
}
