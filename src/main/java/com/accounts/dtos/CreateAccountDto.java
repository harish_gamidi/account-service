package com.accounts.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
public class CreateAccountDto {
    @Getter
    @Setter
    private Long customerId;

    @Getter
    @Setter
    private Double initialCredit;
}
