package com.accounts.repository;

import com.accounts.entity.Account;
import com.accounts.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
}
