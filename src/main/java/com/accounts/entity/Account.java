package com.accounts.entity;

import com.accounts.util.AccountType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Generated;

import javax.persistence.*;
import javax.validation.constraints.Size;


@Entity
@ToString
@Table(name = "ACCOUNTS")
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Getter
    @Setter
    private Long accountId;

    @Getter
    @Setter
    private Double balance;

    @Getter
    @Setter
    private AccountType type;

    @ManyToOne
    @JoinColumn(name = "customerId")
    private Customer customer;

    public Account(AccountType type, Customer customer) {
        this.type = type;
        this.balance = 0.0;
        this.customer = customer;
    }

    public Account(Double initialCredit, AccountType type, Customer customer) {
        this.type = type;
        this.balance = initialCredit;
        this.customer = customer;
    }
}
