package com.accounts.entity;

import com.accounts.util.AccountType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;


@Entity
@ToString
@Table(name = "CUSTOMERS")
@NoArgsConstructor
public class Customer {

    @Id
    @Getter
    @Setter
    private Long customerId;

    @Size(min = 2, max = 30)
    @Getter
    @Setter
    private String firstName;

    @Size(min = 2, max = 30)
    @Getter
    @Setter
    private String lastName;

    @OneToMany(mappedBy = "customer")
    @Getter
    @Setter
    private List<Account> accounts;

    public Customer(@Size(min = 2, max = 30) String firstName, @Size(min = 2, max = 30) String lastName, List<Account> accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    }
}
