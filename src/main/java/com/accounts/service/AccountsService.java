package com.accounts.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.accounts.AccountsApplication;
import com.accounts.entity.Account;
import com.accounts.entity.Customer;
import com.accounts.messaging.events.AccountCreatedEvent;
import com.accounts.repository.AccountRepository;
import com.accounts.repository.CustomerRepository;
import com.accounts.util.AccountType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AccountsService {

    private static List<Account> customers = new ArrayList<Account>();

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public List<Account> getAccounts(Long customerId) {
        Optional<Customer> customer = customerRepository.findById(customerId);
        if (customer.isPresent()) {
            Customer c = customer.get();
            return c.getAccounts();
        }
        return Collections.emptyList();
    }

    public void createNewCurrentAccount(Customer customer, Double initialCredit) {
        Account currentAccount = new Account(initialCredit, AccountType.CURRENT, customer);
        accountRepository.save(currentAccount);

        // publish a message on the exchange
        AccountCreatedEvent accountCreatedEvent = new AccountCreatedEvent();
        accountCreatedEvent.setAccountId(currentAccount.getAccountId());
        accountCreatedEvent.setCustomerId(customer.getCustomerId());
        accountCreatedEvent.setInitialCredit(initialCredit);

        try {
            rabbitTemplate.convertAndSend(AccountsApplication.topicExchangeName,
                    AccountsApplication.routingKey, accountCreatedEvent);
        }
        catch (Exception ex) {
            log.error("Unable to push message to queue");
        }

    }
}
