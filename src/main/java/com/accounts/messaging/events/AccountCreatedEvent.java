package com.accounts.messaging.events;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class AccountCreatedEvent implements Serializable {

    @JsonProperty("accountId")
    Long accountId;

    @JsonProperty("customerId")
    Long customerId;

    @JsonProperty("initialCredit")
    Double initialCredit;
}
