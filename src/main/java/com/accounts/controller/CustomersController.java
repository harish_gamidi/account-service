package com.accounts.controller;

import com.accounts.dtos.CreateAccountDto;
import com.accounts.entity.Account;
import com.accounts.entity.Customer;
import com.accounts.service.AccountsService;
import com.accounts.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CustomersController {

    @Autowired
    private CustomerService customerService;

    @GetMapping(path = "/customers")
    public ResponseEntity getCustomers() {
        List<Customer> customers = customerService.getAll();
        if (customers.isEmpty())
            return new ResponseEntity<>(customers, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }
}
