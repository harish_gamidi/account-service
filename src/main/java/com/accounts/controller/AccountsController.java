package com.accounts.controller;

import com.accounts.dtos.CreateAccountDto;
import com.accounts.entity.Account;
import com.accounts.entity.Customer;
import com.accounts.service.AccountsService;
import com.accounts.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AccountsController {

    @Autowired
    private AccountsService accountsService;

    @Autowired
    private CustomerService customerService;

    @GetMapping(path = "/accounts", produces = "application/json")
    public ResponseEntity getAccounts(@Param("customerId") Long customerId) {

        if (StringUtils.isEmpty(customerId))
            return new ResponseEntity<>("customerId is must", HttpStatus.BAD_REQUEST);

        List<Account> accounts = accountsService.getAccounts(customerId);
        if (accounts.isEmpty())
            return new ResponseEntity<>(accounts, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @PostMapping(path = "/accounts/current", consumes = "application/json")
    public ResponseEntity<String> createNewAccount(@RequestBody CreateAccountDto account) throws Exception {
        Optional<Customer> customer = customerService.getCustomer(account.getCustomerId());
        if (customer.isPresent()) {
            accountsService.createNewCurrentAccount(customer.get(), account.getInitialCredit());
            return new ResponseEntity<>("Account created successfully", HttpStatus.OK);
        }
        throw new Exception("Customer not found");
    }

    @GetMapping(path = "transactions/{customerId}")
    public void save(@PathVariable int customerId, @RequestParam double initialCredit) {
//		service.save(customerId,initialCredit);

    }

}
